﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemigoControlador : MonoBehaviour
    

{
    public bool IsRanged;
    private EnemigoModelo enem;
    private EnemigoVista enev;
    void Start()
    {

        enem = GetComponent<EnemigoModelo>();
      
    }

    // Update is called once per frame
    void Update()
    {
        
    }
  public void MoveToTarget()
    {

        if (enem.navtarget != null)
            enem.navAgent.SetDestination(enem.navtarget.position);
    }
   public void SeekTarget()
    {
        enem.navtarget = GameObject.FindGameObjectWithTag("Player").transform;
    }
    private void FixedUpdate()
    {
        enem.rb.position = (enem.transform.position + enem.navAgent.velocity * Time.fixedDeltaTime);
        enem.navAgent.nextPosition = enem.rb.position;
    }
    public IEnumerator ShootAtPlayer()
    {



        yield return new WaitForSeconds(0.3f);
        if (enem.timer <= 0)
        {
            GameObject bul = Instantiate(enem.bullet, enem.firepoint.position, transform.rotation);
            bul.GetComponent<Rigidbody>().AddForce(enem.firepoint.right * enem.bulletspeed, ForceMode.Impulse);
            enem.timer = enem.fireRate;
        }



    }
    public void LookAtPlayer()
    {
        Vector3 targetDir = enem.player.position - transform.position;

        // The step size is equal to speed times frame time.
        float step = enem.turnSpeed * Time.deltaTime;

        Vector3 newDir = Vector3.RotateTowards(transform.forward, targetDir, step, 0.0f);
        Debug.DrawRay(transform.position, newDir, Color.red);

        // Move our position a step closer to the target.
        transform.rotation = Quaternion.LookRotation(newDir);


    }
    public void Timer()
    {
        enem.timer -= Time.deltaTime;
    }
}
