﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerModelo : MonoBehaviour
{
    public float MoveSpeed;
    public GameObject projectile;
    public Transform shootPoint;
    public float shootspeed;
    public float timer = 0.5f;
    public float fireRate;
    public float maxlife;
    public float life;
    public float _horizontal;
    public float _vertical;
    public Animator anim;
    public float knockbackTime;
    public float knockbackCounter;
    public LayerMask hitmask;

    public Rigidbody rb;
}
