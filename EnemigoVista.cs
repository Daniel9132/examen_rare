﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class EnemigoVista : MonoBehaviour
{
    private EnemigoModelo enem;
    private EnemigoControlador enec;
    private void Start()
    {
        enem = GetComponent<EnemigoModelo>();
        enec = GetComponent<EnemigoControlador>();
        enem.navAgent = GetComponent<NavMeshAgent>();
        enem.rb = GetComponent<Rigidbody>();
        enem.player = GameObject.FindGameObjectWithTag("Player").transform;
        enem.navAgent.updatePosition = false;
        enem.navAgent.updateRotation = true;
        enem.navAgent.stoppingDistance = enem.range;
        enem.life = enem.maxlife;

    }
 void Update()
    {
        
        enec.MoveToTarget();
        if (enem.life <= 0)
        {
            Instantiate(enem.gib,transform.position,transform.rotation);
            Destroy(gameObject);
            return;
        }
        float dist = Vector3.Distance(transform.position, enem.player.transform.position);
        if (dist < enem.seekRange)
        {
            InvokeRepeating("SeekTarget", 0.1f, 0.1f);
        }
        if (dist > enem.seekRange)
        {
            //CancelInvoke("SeekTarget");
            //TargetLost();
            
        }
        if (enec.IsRanged)
        {
        if(enem.navtarget!=null){
         if(enem.navtarget.gameObject.tag=="Player"){
            enem.navAgent.stoppingDistance=9;
            enem.minDistance=9;
            
            enem.isChasing=true;

        }
        }
            enec.Timer();
            float dist1 = Vector3.Distance(transform.position, enem.player.transform.position);
            if (dist1 < enem.seekRange && enem.timer <= 0)
            {
                enec.LookAtPlayer();
                StartCoroutine(enec.ShootAtPlayer());
            }
            if (dist1 > enem.seekRange)
            {
                //TargetLost();
            }
            if (dist1 < enem.seekRange)
            {
                enec.LookAtPlayer();

            }
           
            //Debug.Log(dist1 + " " + enem.seekRange);
        }
        
    }
    void OnCollisionEnter(Collision other)
    {
        if (other.gameObject.tag == "BulletPlayer")
        {
            enem.life -= other.gameObject.GetComponent<Bullet>().damage;
            Damaged();
        }
        if (other.gameObject.tag == "Player")
        {
            other.gameObject.GetComponent<PlayerModelo>().life -= enem.damage;
               Vector3 direction = (transform.position - other.transform.position).normalized;
            other.gameObject.GetComponent<PlayerModelo>().knockbackCounter=0.25f;
            other.gameObject.GetComponent<Rigidbody>().AddForce(direction * -enem.knockbackspeed);
            Debug.Log("HIT");
        }
    }
    void SeekTarget()
    {
        if(GameObject.FindGameObjectWithTag("Player"))
        enem.navtarget = enem.player;
        else
        return;
    }
    void TargetLost()
    {
        enem.navtarget = null;
    }
    void Damaged(){
        enem.navtarget = enem.player;
    }



}
