﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerVista : MonoBehaviour

{
    private PlayerModelo pm;
    private PlayerControlador pc;
    // Start is called before the first frame update
    void Start()
    {
        
        pm = GetComponent<PlayerModelo>();
        pc = GetComponent<PlayerControlador>();
        pm.GetComponent<Rigidbody>();
    }

    // Update is called once per frame
    void Update()
    {
        //MoveInput();
        if (pm.knockbackCounter <= 0)
            pc.MoveInput();
        //MouseInput();
        else
        {
            pm.knockbackCounter -= Time.deltaTime;
        }
        pc.MouseInput();
        pc.Shoot();
        pc.Timer();
        if (pm.life <= 0)
            pc.Die();
        if (pm.rb.velocity == Vector3.zero)
        {
            pm.anim.SetBool("isRunning", false);
        }
        else
        {
            pm.anim.SetBool("isRunning", true);

        }


    }
}
